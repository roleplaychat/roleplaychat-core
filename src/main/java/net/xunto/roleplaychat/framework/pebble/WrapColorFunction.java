package net.xunto.roleplaychat.framework.pebble;


import io.pebbletemplates.pebble.error.PebbleException;
import io.pebbletemplates.pebble.template.EvaluationContext;
import io.pebbletemplates.pebble.template.PebbleTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WrapColorFunction {
    static class Function implements io.pebbletemplates.pebble.extension.Function {
        @Override
        public Object execute(Map<String, Object> args, PebbleTemplate self, EvaluationContext context, int lineNumber) {
            String value = (String) args.getOrDefault("value", "");
            String colorName = (String) args.getOrDefault("colorName", "");

            return PebbleChatTemplate.wrapWithColor(value, colorName);
        }

        @Override
        public List<String> getArgumentNames() {
            List<String> names = new ArrayList<>();
            names.add("value");
            names.add("colorName");
            return names;
        }
    }

    static class Filter implements io.pebbletemplates.pebble.extension.Filter {
        @Override
        public Object apply(Object input, Map<String, Object> args, PebbleTemplate self, EvaluationContext context, int lineNumber) throws PebbleException {
            String colorName = (String) args.getOrDefault("colorName", "");
            return PebbleChatTemplate.wrapWithColor(String.valueOf(input), colorName);
        }

        @Override
        public List<String> getArgumentNames() {
            List<String> names = new ArrayList<>();
            names.add("colorName");
            return names;
        }
    }
}
