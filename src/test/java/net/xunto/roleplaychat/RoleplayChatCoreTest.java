package net.xunto.roleplaychat;

import net.xunto.roleplaychat.framework.api.Request;
import net.xunto.roleplaychat.framework.text.Text;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class RoleplayChatCoreTest extends ChatTest {
    @Test
    public void testNoEscape() {
        String often_escaped = "&<>\"'{}";

        List<Text> result = RoleplayChatCore.instance.process(
                new Request(often_escaped, this.player)
        );

        String text = result.get(0).getUnformattedText();
        assertEquals(
                this.player.getName() + ": " + often_escaped,
                text
        );
    }
}
